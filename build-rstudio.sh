#!/bin/bash

PKGS="gmp4 libffi5 rstudio-desktop-git"
BASEURL="https://aur.archlinux.org/packages/"

CHROOT="/mnt/devel/build/arch"
REPO="/mnt/devel/repo"

rm -f $REPO/i686/*
rm -f $REPO/x86_64/*

cd "$HOME/projects/rstudio-package"

for PKG in $PKGS; do
    rm -rf $PKG
    yaourt -G $PKG
done

for dir in */; do
    cd $dir
    makechrootpkg -u -r $CHROOT/i686 -- -i
    makechrootpkg -u -r $CHROOT/x86_64 -- -i
    rm -f *.log
    cd ../
done

find . -iname "*-i686.pkg.tar.xz" -exec mv -t $REPO/i686/ {} \+
find . -iname "*-x86_64.pkg.tar.xz" -exec mv -t $REPO/x86_64/ {} \+

cd $REPO

for dir in */; do
    cd $dir
    repo-add rstudio.db.tar.gz *.pkg.tar.xz
    repo-add --files rstudio.files.tar.gz *.pkg.tar.xz
    cd ../
done

rsync -rlu --delete -e ssh . root@192.168.1.1:/mnt/sda1/repo/
